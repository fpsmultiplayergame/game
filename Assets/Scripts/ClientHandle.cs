﻿using System.Collections;
using System.Collections.Generic;
using System.Net;
using System;
using UnityEngine;
using UnityEngine.Events;

public class ClientHandle : MonoBehaviour
{

    public DateTime DateTime;
    public static double pingInMiliseconds;
    public static bool finishedConnecting;
    public static bool wasCrouched;

    public static void Welcome(Packet _packet)
    {
        string _msg = _packet.ReadString();
        int _myID = _packet.ReadInt();

        Debug.Log($"Message from server: {_msg}");
        Client.instance.myID = _myID;
        ClientSend.WelcomeReceived();

        // Now that we have the client's id, connect UDP
        Client.instance.udp.Connect(((IPEndPoint)Client.instance.tcp.socket.Client.LocalEndPoint).Port);
        finishedConnecting = true;
    }

    public static void SpawnPlayer(Packet _packet)
    {
        int _id = _packet.ReadInt();
        string _username = _packet.ReadString();
        Vector3 _position = _packet.ReadVector3();
        Quaternion _rotation = _packet.ReadQuaternion();

        GameManager.instance.SpawnPlayer(_id, _username, _position, _rotation);
    }

    public static void PlayerPosition(Packet _packet)
    {
        int _id = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();
        bool _isGrounded = _packet.ReadBool();
        bool _isSneaking = _packet.ReadBool();

        if (GameManager.players.TryGetValue(_id, out PlayerManager _player))
        {
            if (_player.isLocal)
            {
                _player.Move(_position);

                
            }
            else
            {
                _player.transform.position = _position;
                // _player.Move(_position);
            }

            GameManager.instance.SetStates(_player.id, _position, _isGrounded, _isSneaking);
        }
    }

    public static void PlayerRotation(Packet _packet)
    {
        int _id = _packet.ReadInt();
        Quaternion _characterRotation = _packet.ReadQuaternion();
        Quaternion _gunAndArmsRotation = _packet.ReadQuaternion();
        
        if (GameManager.players.TryGetValue(_id, out PlayerManager _player))
        {
            try
            {
                _player.transform.rotation = _characterRotation;
                _player.gun.transform.rotation = _gunAndArmsRotation;
            }
            catch (UnassignedReferenceException) {}
        }
    }

    // TODO instead of this, have animations that make the character lowered
    public static void PlayerCrouched(Packet _packet)
    {
        int _id = _packet.ReadInt();
        bool _isCrouching = _packet.ReadBool();

        if (GameManager.players.TryGetValue(_id, out PlayerManager _player))
        {
            if (_isCrouching && !wasCrouched)
            {
                _player.Crouch(_id, true);
                wasCrouched = true;
            }
            else if (!_isCrouching && wasCrouched)
            {
                _player.Crouch(_id, false);
                wasCrouched = false;
            }
        }
    }

    public static void PlayerDisconnected(Packet _packet)
    {
        int _id = _packet.ReadInt();

        Destroy(GameManager.players[_id].gameObject);
        GameManager.players.Remove(_id);
        GameManager.playersBags.Remove(_id);
        GameManager.playersAnimators.Remove(_id);
    }

    public static void PlayerHealth(Packet _packet)
    {
        int _id = _packet.ReadInt();
        float _health = _packet.ReadFloat();

        GameManager.players[_id].SetHealth(_health);
    }

    public static void PlayerRespawned(Packet _packet)
    {
        int _id = _packet.ReadInt();

        GameManager.players[_id].Respawn();
    }

    public static void CreateItemSpawner(Packet _packet)
    {
        int _spawnerId = _packet.ReadInt();
        Vector3 _spawnerPosition = _packet.ReadVector3();
        bool _hasItem = _packet.ReadBool();

        GameManager.instance.CreateItemSpawner(_spawnerId, _spawnerPosition, _hasItem);
    }

    public static void ItemSpawned(Packet _packet)
    {
        int _spawnerId = _packet.ReadInt();

        GameManager.itemSpawners[_spawnerId].ItemSpawned();
    }

    public static void ItemPickedUp(Packet _packet)
    {
        int _spawnerId = _packet.ReadInt();
        int _byPlayer = _packet.ReadInt();
        GameManager.itemSpawners[_spawnerId].ItemPickedUp();
        GameManager.players[_byPlayer].itemCount++;
    }

    // public static void SpawnProjectile(Packet _packet)
    // {
    //     int _projectileId = _packet.ReadInt();
    //     Vector3 _position = _packet.ReadVector3();
    //     int _thrownByPlayer = _packet.ReadInt();
    //     GameManager.instance.SpawnProjectile(_projectileId, _position);
    //     GameManager.players[_thrownByPlayer].itemCount--;
    // }

    public static void ProjectilePosition(Packet _packet)
    {
        int _projectileId = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();
        if (GameManager.projectiles.TryGetValue(_projectileId, out ProjectileManager _projectile))
        {
            _projectile.transform.position = _position;
        }
    }

    public static void ProjectileExploded(Packet _packet)
    {
        int _projectileId = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();
        
        GameManager.projectiles[_projectileId].Explode(_position);
    }

    public static void SpawnEnemy(Packet _packet)
    {
        int _enemyID = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();

        GameManager.instance.SpawnEnemy(_enemyID, _position);
    }

    public static void EnemyPosition(Packet _packet)
    {
        int _enemyID = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();
        _position.y -= 1f;

        if (GameManager.enemies.TryGetValue(_enemyID, out EnemyManager _enemy))
        {
            _enemy.transform.position = _position;
        }
    }

    public static void EnemyHealth(Packet _packet)
    {
        int _enemyID = _packet.ReadInt();
        float _health = _packet.ReadFloat();

        if (GameManager.enemies.TryGetValue(_enemyID, out EnemyManager _enemy))
        {
            _enemy.SetHealth(_health);
        }
    }

    public static void Ping(Packet _packet)
    {
		DateTimeOffset rttSent = DateTimeOffset.FromUnixTimeMilliseconds(_packet.ReadLong()).DateTime;
		Client.instance.ping = (DateTime.UtcNow - rttSent).Milliseconds;
    }

    public static void BulletSpawn(Packet _packet)
    {
        int _byPlayer = _packet.ReadInt();
        Quaternion _bulletRotation = _packet.ReadQuaternion();
        Vector3 _muzzleWorldVelocity = _packet.ReadVector3();

        GameManager.instance.SpawnBullet(_byPlayer, _bulletRotation, _muzzleWorldVelocity);
    }

    public static void PickupWeaponSpawned(Packet _packet)
    {
        string _name = _packet.ReadString();
		int _weaponID = _packet.ReadInt();
        Vector3 _position = _packet.ReadVector3();
        Quaternion _rotation = _packet.ReadQuaternion();
        Vector3 _positionalVelocity = _packet.ReadVector3();
        Vector3 _rotationalVelocity = _packet.ReadVector3();

        // Spawn, set the position and rotation of that weapon pickup.
		GameManager.instance.SpawnWeaponPickup(_name, _weaponID, _position, _rotation, _positionalVelocity, _rotationalVelocity);
    }

    public static void SwitchedWeapons(Packet _packet)
    {
        string _weaponName = _packet.ReadString();
        int _weaponID = _packet.ReadInt();
        int _pickupID = _packet.ReadInt();
        int _playerID = _packet.ReadInt();
        
        if (_playerID == GameManager.instance.myID)
        {
            // Read the information the gun ammunition
            int _ammoLeft = _packet.ReadInt();
            int _clipAmmoLeft = _packet.ReadInt();

            // Switch weapons of the player and add them if they do not exist
            GameManager.instance.SwitchWeapons(_weaponName, _weaponID, _pickupID, _playerID, _ammoLeft, _clipAmmoLeft);
        }
        else
        {
            // Switch weapons of the player and add them if they do not exist
            GameManager.instance.SwitchWeapons(_weaponName, _weaponID, _pickupID, _playerID);
        }
    }

    public static void GunDropped(Packet _packet)
    {
        int _byPlayerID = _packet.ReadInt();
        int _weaponID = _packet.ReadInt();
        Vector3 _dropOrigin = _packet.ReadVector3();
        Vector3 _position = _packet.ReadVector3();
        Quaternion _rotation = _packet.ReadQuaternion();
        string _currentWeaponName = _packet.ReadString();
        int _currentSlotID = _packet.ReadInt();

        // Handle the dropping of a weapon
        GameManager.instance.DropGun(_byPlayerID, _weaponID, _dropOrigin, _position, _rotation, _currentWeaponName, _currentSlotID);
    }

    public static void GunReloaded(Packet _packet)
    {
        int _playerID = _packet.ReadInt();
        bool _isOutOfAmmo = _packet.ReadBool();

        GameManager.instance.GunReloaded(_playerID, _isOutOfAmmo);
    }
}
