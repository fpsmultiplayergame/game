using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class GunScript : MonoBehaviour {

	//Animator component attached to weapon
	public Animator anim;

	[Header("Cameras")]
	public Camera camera;
    public GameObject cameraPlaceholder;

	[Header("Gun Camera Options")]
	//How fast the camera field of view changes when aiming 
	[Tooltip("How fast the camera field of view changes when aiming.")]
	public float fovSpeed = 15.0f;
	//Default camera field of view
	[Tooltip("Default value for camera field of view (40 is recommended).")]
	public float defaultFov = 37.0f;

	public float aimFov = 15.0f;

	[Header("UI Settings")]
	[Tooltip("Name of the current weapon, shown in the game UI.")]
	public string weaponName;

	[Header("Weapon Sway")]
	//Enables weapon sway
	[Tooltip("Toggle weapon sway.")]
	public bool weaponSway;

	public float swayAmount = 0.02f;
	public float maxSwayAmount = 0.06f;
	public float swaySmoothValue = 4.0f;

	private Vector3 initialSwayPosition;

	//Check if aiming
	private bool isAiming;
	//Check if walking
	private bool isWalking;

	//How much ammo is currently left in the clip
	public int clipAmmo {get; set;}
	//How much ammo is currently left altogether
	public int ammoLeft {get; set;}
	[Tooltip("How much ammo the weapon should have at start.")]
	public int maxAmmo;
	[Tooltip("How much ammo the weapon should have at start.")]
	public int maxClipAmmo;

	[Header("Bullet Settings")]
	//Bullet
	[Tooltip("How long after reloading that the bullet model becomes visible " +
		"again, only used for out of ammo reload animations.")]
	public float showBulletInMagDelay = 0.6f;
	[Tooltip("The bullet model inside the mag, not used for all weapons.")]
	public GameObject bulletInMagRenderer;

	[Header("Grenade Settings")]
	public float grenadeSpawnDelay = 0.35f;

	[Header("Muzzleflash Settings")]
	public bool randomMuzzleflash = false;
	//min should always bee 1
	private int minRandomValue = 1;

	[Range(2, 25)]
	public int maxRandomValue = 5;

	private int randomMuzzleflashValue;

	public bool enableMuzzleflash = true;
	public ParticleSystem muzzleParticles;
	public bool enableSparks = true;
	public ParticleSystem sparkParticles;
	public int minSparkEmission = 1;
	public int maxSparkEmission = 7;

	[Header("Muzzleflash Light Settings")]
	public Light muzzleflashLight;
	public float lightDuration = 0.02f;

	public AudioSource muzzleAudioSource;

	[System.Serializable]
	public class Prefabs
	{  
		[Header("prefabs")]
		public ProjectileBase bulletPrefab;
		public GameObject casingPrefab;
		public GameObject grenadePrefab;
		public GameObject magPrefab;
	}
	public Prefabs prefabs;
	
	[System.Serializable]
	public class SpawnPoints
	{  
		[Header("SpawnPoints")]
		public Transform bulletSpawnPoint;
		public Transform casingSpawnPoint;
		public Transform grenadeSpawnPoint;
		public Transform magSpawnPoint;
	}
	public SpawnPoints spawnPoints;
	
	public WeaponTypeIdentifier m_WeaponTypeIdentifier {get; set;}

	private void Awake () {
		//Set current ammo to total ammo value
		ammoLeft = maxAmmo;
		clipAmmo = maxClipAmmo;

		m_WeaponTypeIdentifier = GetComponent<WeaponTypeIdentifier>();
	}

	private void Start () {

		//Weapon sway
		initialSwayPosition = transform.localPosition;
	}

	private void LateUpdate () {
		
		//Weapon sway
		if (weaponSway == true) 
		{
			float movementX = -Input.GetAxis ("Mouse X") * swayAmount;
			float movementY = -Input.GetAxis ("Mouse Y") * swayAmount;
			//Clamp movement to min and max values
			movementX = Mathf.Clamp 
				(movementX, -maxSwayAmount, maxSwayAmount);
			movementY = Mathf.Clamp 
				(movementY, -maxSwayAmount, maxSwayAmount);
			//Lerp local pos
			Vector3 finalSwayPosition = new Vector3 
				(movementX, movementY, 0);
			transform.localPosition = Vector3.Lerp 
				(transform.localPosition, finalSwayPosition + 
					initialSwayPosition, Time.deltaTime * swaySmoothValue);
		}
	}
	
	private void Update () {

		// //Aiming
		// //Toggle camera FOV when right click is held down
		// if(Input.GetButton("Aim") && !isReloading) 
		// {
			
		// 	isAiming = true;
		// 	//Start aiming
		// 	anim.SetBool ("Aim", true);

		// 	//When right click is released
		// 	gunCamera.fieldOfView = Mathf.Lerp(gunCamera.fieldOfView,
		// 		aimFov,fovSpeed * Time.deltaTime);

		// 	if (!soundHasPlayed) 
		// 	{
		// 		// AudioManager.instance.PlayAimSound(id);
	
		// 		soundHasPlayed = true;
		// 	}
		// } 
		// else 
		// {
		// 	//When right click is released
		// 	gunCamera.fieldOfView = Mathf.Lerp(gunCamera.fieldOfView,
		// 		defaultFov,fovSpeed * Time.deltaTime);

		// 	isAiming = false;
		// 	//Stop aiming
		// 	anim.SetBool ("Aim", false);
				
		// 	soundHasPlayed = false;
		// }
		// //Aiming end

		//If randomize muzzleflash is true, genereate random int values
		if (randomMuzzleflash == true) 
		{
			randomMuzzleflashValue = Random.Range (minRandomValue, maxRandomValue);
		}

		//Set current ammo text from ammo int
		// m_playerController.currentAmmoText.text = currentAmmo.ToString();

		//Continosuly check which animation 
		//is currently playing
		// AnimationCheck ();

		// //Play knife attack 1 animation when Q key is pressed
		// if (Input.GetKeyDown (KeyCode.Q)) 
		// {
		// 	anim.Play ("Knife Attack 1", 0, 0f);
		// }
		// //Play knife attack 2 animation when F key is pressed
		// if (Input.GetKeyDown (KeyCode.F)) 
		// {
		// 	anim.Play ("Knife Attack 2", 0, 0f);
		// }

		// 	// TODO Implement properly
		// 	// StartCoroutine (GrenadeSpawnDelay ());
		// 	//Play grenade throw animation
		// 	anim.Play("GrenadeThrow", 0, 0.0f);
		// }

		//If out of ammo
		// if (ammoLeft == 0) 
		// {
			//Show out of ammo text
			// m_playerController.currentWeaponText.text = "OUT OF AMMO";
			//Toggle bool
			// outOfAmmo = true;
		// } 
		// else 
		// {
			//When ammo is full, show weapon name again
			// m_playerController.currentWeaponText.text = storedWeaponName.ToString ();
			//Toggle bool
			// outOfAmmo = false;
			//anim.SetBool ("Out Of Ammo", false);
		// }

		// 	mainAudioSource.clip = SoundClips.takeOutSound;
		// 	mainAudioSource.Play ();
		
		// }

		// TODO move stuff over to PlayerManager
		// //Walking when pressing down WASD keys
		// if (Input.GetKey (KeyCode.W) && !isRunning || 
		// 	Input.GetKey (KeyCode.A) && !isRunning || 
		// 	Input.GetKey (KeyCode.S) && !isRunning || 
		// 	Input.GetKey (KeyCode.D) && !isRunning) 
		// {
		// 	anim.SetBool ("Walk", true);
		// } else {
		// 	anim.SetBool ("Walk", false);
		// }
		
		// //Run anim toggle
		// if (isRunning == true) 
		// {
		// 	anim.SetBool ("Run", true);
		// } 
		// else 
		// {
		// 	anim.SetBool ("Run", false);
		// }
	}

	private IEnumerator GrenadeSpawnDelay () {
		
		//Wait for set amount of time before spawning grenade
		yield return new WaitForSeconds (grenadeSpawnDelay);
		//Spawn grenade prefab at spawnpoint
		Instantiate(prefabs.grenadePrefab, 
			spawnPoints.grenadeSpawnPoint.transform.position, 
			spawnPoints.grenadeSpawnPoint.transform.rotation);
	}

	// //Reload
	// public void Reload () {
		
	// 	if (outOfAmmo == true) 
	// 	{
	// 		//Play diff anim if out of ammo
	// 		anim.Play ("Reload Out Of Ammo", 0, 0f);

	// 		//If out of ammo, hide the bullet renderer in the mag
	// 		//Do not show if bullet renderer is not assigned in inspector
	// 		if (bulletInMagRenderer != null) 
	// 		{
	// 			bulletInMagRenderer.GetComponent
	// 			<SkinnedMeshRenderer> ().enabled = false;
	// 			//Start show bullet delay
	// 			StartCoroutine (ShowBulletInMag ());
	// 		}
	// 	} 
	// 	else 
	// 	{
	// 		//Play diff anim if ammo left
	// 		anim.Play ("Reload Ammo Left", 0, 0f);

	// 		//If reloading when ammo left, show bullet in mag
	// 		//Do not show if bullet renderer is not assigned in inspector
	// 		if (bulletInMagRenderer != null) 
	// 		{
	// 			bulletInMagRenderer.GetComponent
	// 			<SkinnedMeshRenderer> ().enabled = true;
	// 		}
	// 	}
	// 	//Restore ammo when reloading
	// 	currentAmmo = ammo;
	// 	outOfAmmo = false;
	// }

	//Enable bullet in mag renderer after set amount of time
	private IEnumerator ShowBulletInMag () {
		
		//Wait set amount of time before showing bullet in mag
		yield return new WaitForSeconds (showBulletInMagDelay);
		bulletInMagRenderer.GetComponent<SkinnedMeshRenderer> ().enabled = true;
	}

	//Show light when shooting, then disable after set amount of time
	private IEnumerator MuzzleFlashLight () {
		
		muzzleflashLight.enabled = true;
		yield return new WaitForSeconds (lightDuration);
		muzzleflashLight.enabled = false;
	}

	// //Check current animation playing
	// private void AnimationCheck () {
		
	// 	//Check if reloading
	// 	//Check both animations
	// 	if (anim.GetCurrentAnimatorStateInfo (0).IsName ("Reload Out Of Ammo") || 
	// 		anim.GetCurrentAnimatorStateInfo (0).IsName ("Reload Ammo Left")) 
	// 	{
	// 		isReloading = true;
	// 	} 
	// 	else 
	// 	{
	// 		isReloading = false;
	// 	}
	// }

	public void ShootBullet(Vector3 _position, Quaternion _rotation, GameObject _playerParent, int _byPlayerID, Vector3 _cameraForward, Vector3 _muzzleWorldVelocity)
	{
		// Spawn a bullet projectile, also add velocity to it
		ProjectileBase newProjectile = Instantiate(prefabs.bulletPrefab, _position, _rotation);
        newProjectile.Shoot(_playerParent, _byPlayerID, _position, _cameraForward, _muzzleWorldVelocity);

		// Do effects
		//If random muzzle is false
		if (!randomMuzzleflash && 
			enableMuzzleflash == true) 
		{
			muzzleParticles.Emit (1);
			//Light flash start
			StartCoroutine(MuzzleFlashLight());
		} 
		else if (randomMuzzleflash == true)
		{
			//Only emit if random value is 1
			if (randomMuzzleflashValue == 1) 
			{
				if (enableSparks == true) 
				{
					//Emit random amount of spark particles
					sparkParticles.Emit (Random.Range (minSparkEmission, maxSparkEmission));
				}
				if (enableMuzzleflash == true) 
				{
					muzzleParticles.Emit (1);
					//Light flash start
					StartCoroutine (MuzzleFlashLight ());
				}
			}
		}
		
		//Spawn casing prefab
		Instantiate (prefabs.casingPrefab, 
			spawnPoints.casingSpawnPoint.transform.position, 
			spawnPoints.casingSpawnPoint.transform.rotation);
	}

	public void SpawnMag()
	{
		//Spawn casing prefab
		Instantiate (prefabs.magPrefab, 
			spawnPoints.magSpawnPoint.transform.position, 
			spawnPoints.magSpawnPoint.transform.rotation);
	}
}