﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleWeaponPickup : MonoBehaviour
{
    public Rigidbody pickupRigidbody { get; private set; }
    Collider m_Collider;

    bool run = true;

    void Start()
    {
        pickupRigidbody = GetComponent<Rigidbody>();

        m_Collider = GetComponent<Collider>();
    }

    void Update()
    {
        Debug.DrawRay(transform.position, -transform.up * 0.2f, Color.red);
        Debug.DrawRay(transform.position, transform.up * 0.2f, Color.red);
        Debug.DrawRay(transform.position, -transform.forward * 0.2f, Color.red);
        Debug.DrawRay(transform.position, transform.forward * 0.2f, Color.red);
        Debug.DrawRay(transform.position, -transform.right * 0.2f, Color.red);
        Debug.DrawRay(transform.position, transform.right * 0.2f, Color.red);

        if (run)
        {
            // ASSUMES THAT THE GROUND IS FLAT, AND THAT IT'S LOCATED AT Y -1!!!
            if (gameObject.transform.position.y <= -0.5f)
            {
                run = false;
                pickupRigidbody.velocity = new Vector3(0f, 0f, 0f); 
                pickupRigidbody.angularVelocity = new Vector3(0f, 0f, 0f); 
                m_Collider.attachedRigidbody.useGravity = false;
                m_Collider.attachedRigidbody.isKinematic = true;
                gameObject.transform.rotation =  Quaternion.Euler(0f, gameObject.transform.rotation.y*100, gameObject.transform.rotation.z*100);
                gameObject.transform.position = new Vector3(gameObject.transform.position.x, -0.849f, gameObject.transform.position.z);
            }
        }
    }
}
