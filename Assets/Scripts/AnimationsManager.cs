﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationsManager : MonoBehaviour
{
    public static AnimationsManager instance;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.Log("Instance already exists, destroying object!");
            Destroy(this);
        }
    }

    private Animator GetAnimator(int _id)
    {
        Animator _anim = GameManager.playersAnimators[_id];
        return _anim;
    }

    private void GetClipLength(int _id, Animator _anim, string _weaponName, bool _out)
    {
        float _waitFor = 0f;

        if (_weaponName == "AK-47 Rifle")
        {
            if (_out)
            {
                _waitFor =  AudioManager.instance.soundClipLengths.AK47ReloadSoundOutOfAmmo;
            }
            else
            {
                _waitFor =  AudioManager.instance.soundClipLengths.AK47ReloadSoundAmmoLeft;
            }
        }

        else if (_weaponName == "Glock-18")
        {
            if (_out)
            {
                _waitFor = AudioManager.instance.soundClipLengths.glockReloadSoundOutOfAmmo;
            }
            else
            {
                _waitFor = AudioManager.instance.soundClipLengths.glockReloadSoundAmmoLeft;
            }
        }

        StartCoroutine(StopAnimation(_anim, _waitFor));
    }

    private IEnumerator StopAnimation(Animator _anim, float _waitFor)
    {
        yield return new WaitForSeconds(_waitFor);

        // In case someone has started reloading the gun and then threw it, an exception would occur. Should be faster than checking if anim is null.
        try
        {
            _anim.Play ("Idle");
        }
        catch (MissingReferenceException) {}
    }

    public void PlayTakeOut(int _id)
    {
        Animator anim = GetAnimator(_id);
        anim.SetBool ("Holster", false);
    }

    public void PlayFireAnimation(int _id)
    {
        Animator anim = GetAnimator(_id);
        anim.Play ("Fire", 0, 0f);
    }

    public void PlayReloadOutOfAmmoAnimation(int _id, string _weaponName)
    {
        Animator anim = GetAnimator(_id);
        anim.Play ("Reload Out Of Ammo", 0, 0f);
        GetClipLength(_id, anim, _weaponName, true);
    }

    public void PlayReloadAmmoLeftAnimation(int _id, string _weaponName)
    {
        Animator anim = GetAnimator(_id);
        anim.Play ("Reload Ammo Left", 0, 0f);
        GetClipLength(_id, anim, _weaponName, false);
    }
}
