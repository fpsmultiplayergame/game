﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerManager : MonoBehaviour
{
    public int id;
    public string username;
    public float health;
    public float maxHealth = 100f;
    public int itemCount = 0;
    public string currentWeaponName;

    public GameObject armsRoot;
    public MeshRenderer characterModel;
    public GameObject gun;

    [Tooltip("Parent transform where a current weapon lies")]
    public Transform weaponParentSocket;
    [Tooltip("Parent transform where a primary weapon will be added in the hierarchy")]
    public Transform primaryWeaponParentSocket;
    [Tooltip("Parent transform where a secondary weapon will be added in the hierarchy")]
    public Transform secondaryWeaponParentSocket;
    [Tooltip("Parent transform where a melee weapon will be added in the hierarchy")]
    public Transform meleeWeaponParentSocket;
    [Tooltip("Parent transform where a grenade weapon will be added in the hierarchy")]
    public Transform grenadeWeaponParentSocket;
    [Tooltip("Parent transform where a smoke weapon will be added in the hierarchy")]
    public Transform smokeWeaponParentSocket;
    [Tooltip("Parent transform where a flash weapon will be added in the hierarchy")]
    public Transform flashWeaponParentSocket;
    
    private Interpolator interpolator;
    public PlayerController playerController;
    
    public AudioSource mainAudioSource;
    public AudioSource muzzleAudioSource;

    public bool isLocal = false;

    public void Initialize(int _id, string _username)
    {
        id = _id;
        username = _username;
        health = maxHealth;
        
        CheckIfLocal();
        interpolator = GetComponent<Interpolator>();
        mainAudioSource = GetComponent<AudioSource>();
    }

    public void CheckIfLocal()
    {
        // Check if this object has a script called PlayerController, if yes then it's our local player
        playerController = GetComponent<PlayerController>();
        if (playerController != null)
        {
            isLocal = true;
        }
    }

    // TODO play an crouching animation.
    public void Crouch(int _id, bool _crouchState)
    {
        if (id == _id)
        {            
            // animation ?
        }
        else if (GameManager.players.TryGetValue(_id, out PlayerManager _player))
        {
            // If it's status for other players send it to them, but keep in mind it's then not a local prefab.
            _player.Crouch(_id, _crouchState);
        }
    }

    public void SetHealth(float _health)
    {
        health = _health;

        Debug.Log(
            string.Format("Health of {0} is: {1}", username, _health)
        );

        if (health <= 0f)
        {
            Die();
        }
    }

    public void Die()
    {
        characterModel.enabled = false;
        List<SkinnedMeshRenderer> meshes = new List<SkinnedMeshRenderer>(gun.GetComponentsInChildren<SkinnedMeshRenderer>());
        foreach (SkinnedMeshRenderer mesh in meshes)
        {
            mesh.enabled = false;
        }
    }

    public void Respawn()
    {
        characterModel.enabled = true;
        List<SkinnedMeshRenderer> meshes = new List<SkinnedMeshRenderer>(gun.GetComponentsInChildren<SkinnedMeshRenderer>());
        foreach (SkinnedMeshRenderer mesh in meshes)
        {
            mesh.enabled = true;
        }

        SetHealth(maxHealth);
    }

    public void Move(Vector3 _position)
    {
        interpolator.NewUpdate(GameConstants.ticksPerSecond, _position);
    }
}
