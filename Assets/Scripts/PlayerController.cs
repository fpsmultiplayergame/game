﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    // public Camera gunCamera;
    // public Transform gunCameraTransform;
    [Tooltip("Speed of crouching transitions")]
    public float crouchingSharpness = 10f;

    [Header("UI Components")]
	public Text currentWeaponText;
	public Text clipAmmoText;
    public Text ammoDividerText;
	public Text ammoLeftText;
    public GameObject AK47WeaponIcon;
    public GameObject glockWeaponIcon;
    
    public PlayerManager m_PlayerManager;

    private void Start()
    {
        m_PlayerManager = GetComponent<PlayerManager>();
    }

    private void FixedUpdate()
    {
        if (Input.GetButton(GameConstants.k_ButtonNameFire))
        {
            // shoot
            ClientSend.PlayerShoot();
        }

        if (Input.GetButton(GameConstants.k_ButtonNameDrop))
        {
            // Drop weapon
            ClientSend.DropGun();
        }

        // TODO aim
        if (Input.GetButton(GameConstants.k_ButtonNameAim))
        {
            // AIM
        }

        SendInputToServer();
    }

    private bool CheckIfCanReload()
    {
        if (GameManager.instance.CanReload(m_PlayerManager.id) && Input.GetButton(GameConstants.k_ButtonNameReload))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    /// <summary>Sends player input to the server.</summary>
    private void SendInputToServer()
    {
        bool _reloadState = CheckIfCanReload();

        bool[] _inputs = new bool[]
        {
            Input.GetButton(GameConstants.k_ButtonNameJump),
            Input.GetButton(GameConstants.k_ButtonNameSneak),
            Input.GetButton(GameConstants.k_ButtonNameCrouch),
            _reloadState,

            Input.GetKey(KeyCode.Alpha1),
            Input.GetKey(KeyCode.Alpha2),
            Input.GetKey(KeyCode.Alpha3),
            Input.GetKey(KeyCode.Alpha4),
        };

        float _vertical = Input.GetAxisRaw(GameConstants.k_AxisNameVertical);
        float _horizontal = Input.GetAxisRaw(GameConstants.k_AxisNameHorizontal);

        float _switchWeapon = Input.GetAxisRaw(GameConstants.k_ButtonNameSwitchWeapon);
        float _nextWeapon = Input.GetAxisRaw(GameConstants.k_ButtonNameNextWeapon);

        ClientSend.PlayerMovement(_inputs, _vertical, _horizontal, _switchWeapon, _nextWeapon);
    }
}
