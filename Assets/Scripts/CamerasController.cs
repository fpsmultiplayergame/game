﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine;

public class CamerasController : MonoBehaviour
{
    public PlayerManager playerManager;
    public PlayerController playerController;
    [Tooltip("Sensitivity multiplier for moving the camera around")]
    public float lookSensitivity = 2f;
    public float clampAngleBeginning = -89f;
    public float clampAngleEnd = 89f;
    public float m_CameraVerticalAngle = 0f;
    public float m_CameraHorizontalAngle = 0f;

    [Tooltip("Used to flip the vertical input axis")]
    public bool invertYAxis = false;

    [Header("Rotation")]
    [Tooltip("Rotation speed for moving the camera")]
    public float rotationSpeed = 200f;
    [Range(0.1f, 1f)]
    [Tooltip("Rotation speed multiplier when aiming")]
    public float aimingRotationMultiplier = 0.4f;

    public UnityAction<float> updateCameras;

    public float RotationMultiplier
    {
        get
        {
            // TODO handle aiming
            //if (m_WeaponsManager.isAiming)
            //{
            //    return aimingRotationMultiplier;
            //}

            return 1f;
        }
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Escape))
        {
            ToggleCursorMode();
        }

        if (Cursor.lockState == CursorLockMode.Locked)
        {
            Look();
        }

        if (playerManager.gun != null)
        {
            Debug.DrawRay(playerManager.gun.transform.position, playerManager.gun.transform.forward * 2, Color.red);
        }
    }
    
    private void Look()
    {
        // horizontal character rotation
        {
            // rotate the transform with the input speed around its local Y axis
            playerManager.transform.Rotate(new Vector3(0f, (GetLookInputsHorizontal() * rotationSpeed * RotationMultiplier * -1f), 0f), Space.Self);
        }

        // vertical camera rotation
        {
            // add vertical inputs to the camera's vertical angle
            m_CameraVerticalAngle += GetLookInputsVertical() * rotationSpeed * RotationMultiplier;
            
            // limit the camera's vertical angle to min/max
            m_CameraVerticalAngle = Mathf.Clamp(m_CameraVerticalAngle, clampAngleBeginning, clampAngleEnd);

            // apply the vertical angle as a local rotation to the camera transform along its right axis (makes it pivot up and down)
            if (updateCameras != null)
            {
                updateCameras.Invoke(m_CameraVerticalAngle);
            }
        }
    }

    public float GetLookInputsHorizontal()
    {
        return GetMouseLookAxis(GameConstants.k_MouseAxisNameHorizontal);
    }

    public float GetLookInputsVertical()
    {
        return GetMouseLookAxis(GameConstants.k_MouseAxisNameVertical);
    }

    float GetMouseLookAxis(string mouseInputName)
    {
        if (CanProcessInput())
        {
            float i = Input.GetAxisRaw(mouseInputName);

            // handle inverting vertical input
            if (!invertYAxis)
                i *= -1f;

            // apply sensitivity multiplier
            i *= lookSensitivity * 0.001f;

            return i;
        }
        return 0f;
    }

    public bool CanProcessInput()
    {
        return Cursor.lockState == CursorLockMode.Locked;
    }

    private void ToggleCursorMode()
    {
        Cursor.visible = !Cursor.visible;

        if (Cursor.lockState == CursorLockMode.None)
        {
            Cursor.lockState = CursorLockMode.Locked;
        }
        else
        {
            Cursor.lockState = CursorLockMode.None;
        }
    }
}
