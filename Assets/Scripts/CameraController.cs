﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class CameraController : MonoBehaviour
{
    CamerasController camerasController;
    public GameObject weaponPrefab;

    private void Start()
    {
        PlayerManager m_Player = GameManager.players[GameManager.instance.myID];
        camerasController = m_Player.armsRoot.GetComponent<CamerasController>();

        // Subscribe to updateCameras action
        camerasController.updateCameras += UpdateCameraPosition;
    }

    private void UpdateCameraPosition(float m_CameraVerticalAngle)
    {
        weaponPrefab.transform.localRotation = Quaternion.Euler(m_CameraVerticalAngle, 0f, 0f);
    }

    private void OnDestroy()
    {
        // Unsubscribe
        try
        {
            camerasController.updateCameras -= UpdateCameraPosition;
        }
        catch (NullReferenceException) {}
    }
}
