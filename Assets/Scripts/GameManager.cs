﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using UnityEngine.UI;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public static Dictionary<int, PlayerManager> players = new Dictionary<int, PlayerManager>();
    public static Dictionary<int, ItemSpawner> itemSpawners = new Dictionary<int, ItemSpawner>();
    public static Dictionary<int, ProjectileManager> projectiles = new Dictionary<int, ProjectileManager>();
    public static Dictionary<int, EnemyManager> enemies = new Dictionary<int, EnemyManager>();
	public static Dictionary<int, GameObject> weaponPickups = new Dictionary<int, GameObject>();
    public static Dictionary<string, GameObject> weaponPickupObjectNames = new Dictionary<string, GameObject>();
    public static Dictionary<string, GameObject> startingWeaponObjectNames = new Dictionary<string, GameObject>();
    public static Dictionary<int, List<GameObject>> playersBags = new Dictionary<int, List<GameObject>>();
    public static Dictionary<int, string> previousPlayerWeapon = new Dictionary<int, string>();
    public List<GameObject> weaponPickupObjects = new List<GameObject>();
    public List<GameObject> startingWeaponObjects = new List<GameObject>();
    public Dictionary<int, Vector3> previousPlayerPosition = new Dictionary<int, Vector3>();
    public Dictionary<int, bool> previousGroundedState = new Dictionary<int, bool>();
    public static Dictionary<int, Animator> playersAnimators = new Dictionary<int, Animator>();

    public GameObject localPlayerPrefab;
    public GameObject playerPrefab;
    public GameObject itemSpawnerPrefab;
    public GameObject projectilePrefab;
    public GameObject enemyPrefab;
    public int myID;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.Log("Instance already exists, destroying object!");
            Destroy(this);
        }
    }

    private void Start()
    {
        foreach (GameObject _pickupObject in weaponPickupObjects)
        {
            weaponPickupObjectNames.Add(_pickupObject.name.ToLower(), _pickupObject);
        }

        foreach (GameObject _startingWeaponObject in startingWeaponObjects)
        {
            startingWeaponObjectNames.Add(_startingWeaponObject.name.ToLower(), _startingWeaponObject);
        }
    }

    public void SetSocket(int index, PlayerManager m_Player)
    {
        try
        {
            // Depending on the weapon type, add the weapon to a specific socket in the bag
            if (index == 1)
            {
                m_Player.weaponParentSocket = m_Player.primaryWeaponParentSocket;
            }
            else if (index == 2)
            {
                m_Player.weaponParentSocket = m_Player.secondaryWeaponParentSocket;
            }
            else if (index == 3)
            {
                m_Player.weaponParentSocket = m_Player.meleeWeaponParentSocket;
            }
            else if (index == 4)
            {
                m_Player.weaponParentSocket = m_Player.grenadeWeaponParentSocket;
            }
            else if (index == 5)
            {
                m_Player.weaponParentSocket = m_Player.smokeWeaponParentSocket;
            }
            else if (index == 6)
            {
                m_Player.weaponParentSocket = m_Player.flashWeaponParentSocket;
            }
            else
            {
                throw new ArgumentException();
            }
        }
        catch (ArgumentException)
        {
            return;
        }
    }

    private void GetNewClipAmmo(PlayerManager m_Player, GunScript _gunScript, int _currentWeaponType, bool _subtract = true, string _actualWeaponName = "", int _ammoLeft = 0, int _clipAmmo = 0)
    {
        if (_subtract)
        {
            _ammoLeft = _gunScript.ammoLeft;
            _clipAmmo = _gunScript.clipAmmo;
            _clipAmmo = Mathf.Clamp(_clipAmmo - 1, 0, 999);
            _gunScript.clipAmmo = _clipAmmo;
        }
        else
        {
            _gunScript.ammoLeft = _ammoLeft;
            _gunScript.clipAmmo = _clipAmmo;
        }

        m_Player.playerController.currentWeaponText.text = _actualWeaponName;

        if (_currentWeaponType == 3)
        {
            m_Player.playerController.ammoLeftText.gameObject.SetActive(false);
            m_Player.playerController.ammoDividerText.gameObject.SetActive(false);
            m_Player.playerController.clipAmmoText.gameObject.SetActive(false);
        }
        else
        {
            m_Player.playerController.ammoLeftText.text = _ammoLeft.ToString();
            m_Player.playerController.clipAmmoText.text = _clipAmmo.ToString();
            m_Player.playerController.ammoLeftText.gameObject.SetActive(true);
            m_Player.playerController.ammoDividerText.gameObject.SetActive(true);
            m_Player.playerController.clipAmmoText.gameObject.SetActive(true);
        }
    }

    private float PlayAppropriateAudioAndAnimation(PlayerManager _playerManager, GunScript _gunScript, string _weaponName, string _clipName)
    {
        int _playerID = _playerManager.id;
        PlayerController _playerController = _playerManager.playerController;
        
        if (_weaponName == "AK-47 Rifle")
        {
            if (_clipName == "take out")
            {
                AudioManager.instance.PlayAK47TakeOut(_playerID);
                AnimationsManager.instance.PlayTakeOut(_playerID);

                if (_playerManager.isLocal)
                {
                    // Also switch to the icon, since we're already here.
                    _playerController.glockWeaponIcon.SetActive(false);
                    _playerController.AK47WeaponIcon.SetActive(true);
                }
            }
            else if (_clipName == "shoot")
            {
                AudioManager.instance.PlayAK47ShootSound(_playerID);
                AnimationsManager.instance.PlayFireAnimation(_playerID);
            }
            else if (_clipName == "reload")
            {
                AudioManager.instance.PlayAK47ReloadSoundAmmoLeft(_playerID);
                AnimationsManager.instance.PlayReloadAmmoLeftAnimation(_playerID, _weaponName);

                _gunScript.SpawnMag();
                return AudioManager.instance.soundClipLengths.AK47ReloadSoundAmmoLeft;
            }
            else if (_clipName == "reload empty")
            {
                AudioManager.instance.PlayAK47ReloadSoundOutOfAmmo(_playerID);
                AnimationsManager.instance.PlayReloadOutOfAmmoAnimation(_playerID, _weaponName);

                _gunScript.SpawnMag();
                return AudioManager.instance.soundClipLengths.AK47ReloadSoundOutOfAmmo;
            }
        }

        else if (_weaponName == "Glock-18")
        {
            if (_clipName == "take out")
            {
                AudioManager.instance.PlayGlockTakeOut(_playerID);
                AnimationsManager.instance.PlayTakeOut(_playerID);

                if (_playerManager.isLocal)
                {
                    // Also switch to the icon, since we're already here.
                    _playerController.AK47WeaponIcon.SetActive(false);
                    _playerController.glockWeaponIcon.SetActive(true);
                }
            }
            else if (_clipName == "shoot")
            {
                AudioManager.instance.PlayGlockShootSound(_playerID);
                AnimationsManager.instance.PlayFireAnimation(_playerID);
            }
            else if (_clipName == "reload")
            {
                AudioManager.instance.PlayGlockReloadSoundAmmoLeft(_playerID);
                AnimationsManager.instance.PlayReloadAmmoLeftAnimation(_playerID, _weaponName);

                _gunScript.SpawnMag();
                return AudioManager.instance.soundClipLengths.glockReloadSoundAmmoLeft;
            }
            else if (_clipName == "reload empty")
            {
                AudioManager.instance.PlayGlockReloadSoundOutOfAmmo(_playerID);
                AnimationsManager.instance.PlayReloadOutOfAmmoAnimation(_playerID, _weaponName);

                _gunScript.SpawnMag();
                return AudioManager.instance.soundClipLengths.glockReloadSoundOutOfAmmo;
            }
        }
        
        else if (_playerManager.isLocal)
        {
            _playerController.AK47WeaponIcon.SetActive(false);
            _playerController.glockWeaponIcon.SetActive(false);
        }
        return 0f;
    }



    // ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------




    /// <summary>Spawns a player.</summary>
    /// <param name="_id">The player's ID.</param>
    /// <param name="_name">The player's name.</param>
    /// <param name="_position">The player's starting position.</param>
    /// <param name="_rotation">The player's starting rotation.</param>
    public void SpawnPlayer(int _id, string _username, Vector3 _position, Quaternion _rotation)
    {
        GameObject _player;
        if (_id == Client.instance.myID)
        {
            _player = Instantiate(localPlayerPrefab, _position, _rotation);
            myID = _id;
        }
        else
        {
            _player = Instantiate(playerPrefab, _position, _rotation);
        }

        _player.GetComponent<PlayerManager>().Initialize(_id, _username);
        players.Add(_id, _player.GetComponent<PlayerManager>());
        previousPlayerPosition.Add(_id, Vector3.zero);
        previousGroundedState.Add(_id, true);
    }

    public void CreateItemSpawner(int _spawnerId, Vector3 _position, bool _hasItem)
    {
        GameObject _spawner = Instantiate(itemSpawnerPrefab, _position, itemSpawnerPrefab.transform.rotation);
        _spawner.GetComponent<ItemSpawner>().Initialize(_spawnerId, _hasItem);
        itemSpawners.Add(_spawnerId, _spawner.GetComponent<ItemSpawner>());
    }

    public void SpawnProjectile(int _id, Vector3 _position)
    {
        GameObject _projectile = Instantiate(projectilePrefab, _position, Quaternion.identity);
        _projectile.GetComponent<ProjectileManager>().Initialize(_id);
        projectiles.Add(_id, _projectile.GetComponent<ProjectileManager>());
    }

    public void SpawnEnemy(int _id, Vector3 _position)
    {
        _position.y -= 1f;
        GameObject _enemy = Instantiate(enemyPrefab, _position, Quaternion.identity);
        _enemy.GetComponent<EnemyManager>().Initialize(_id);
        enemies.Add(_id, _enemy.GetComponent<EnemyManager>());
    }
	
	public void SpawnWeaponPickup(string _name, int _weaponID, Vector3 _position, Quaternion _rotation, Vector3 _positionalVelocity, Vector3 _rotationalVelocity)
	{
        if (_position.y-0.101f > 0f)
        {
            _position = new Vector3(_position.x, _position.y-0.101f, _position.z);
        }
        _position.y -= 1f;
		GameObject _weaponPickup = Instantiate(weaponPickupObjectNames[_name], _position, _rotation);
        _weaponPickup.GetComponent<Rigidbody>().velocity = _positionalVelocity;
        _weaponPickup.GetComponent<Rigidbody>().angularVelocity = _rotationalVelocity;

		weaponPickups.Add(_weaponID, _weaponPickup);
	}

    public void AddWeapon(string _weaponName, int _pickupID, int _playerID)
    {
        Debug.Log("Inside AddWeapon, weapon: "+_weaponName);
        GameObject _weaponObject;
        GameObject _weaponPrefabInstance;
        PlayerManager m_Player = players[_playerID];
        
        // Get a local version of the prefab ready for spawning
        try
        {
            _weaponPrefabInstance = startingWeaponObjectNames[_weaponName];
            Debug.Log("1");
        }
        catch
        {
            _weaponPrefabInstance = weaponPickupObjectNames[_weaponName+" pickup"].GetComponent<WeaponProperties>().weaponPrefab;
            Debug.Log("2");
        }

        WeaponTypeIdentifier m_WeaponTypeIdentifier = _weaponPrefabInstance.GetComponent<WeaponTypeIdentifier>();

        // Change the current parent weapon socket transform.
        m_WeaponTypeIdentifier.SetValue();
        int _weaponType = m_WeaponTypeIdentifier.currentWeaponType;
        SetSocket(_weaponType, m_Player);

        // Spawn the new arms with the weapon
        _weaponObject = Instantiate(_weaponPrefabInstance, m_Player.weaponParentSocket);

        // Disable every other arms with a weapon
        foreach (KeyValuePair<int, List<GameObject>> _playersBag in playersBags)
        {
            if (_playersBag.Key == _playerID)
            {
                foreach (GameObject _weaponBagObject in _playersBag.Value)
                {
                    _weaponBagObject.SetActive(false);
                }
            }
        }

        // Change the player's current arms with weapon
        m_Player.gun = _weaponObject;

        // Add the weapon to the dictionary so we can later disable and if necessary destroy the same arms and weapon.
        try
        {
            playersBags[_playerID].Add(_weaponObject);
        }
        catch (KeyNotFoundException)
        {
            List<GameObject> _tempList = new List<GameObject>();
            _tempList.Add(_weaponObject);
            playersBags.Add(_playerID, _tempList);
        }

        if (weaponPickups.TryGetValue(_pickupID, out GameObject _weaponPickupObject))
        {
            // Destroy the weapon pickup object
            Destroy(_weaponPickupObject);
            
            // Remove the weapon pickup object from the dictionary
            weaponPickups.Remove(_pickupID);
        }
    }

    private void AddThenSwitch(PlayerManager m_Player, string _weaponName, int _weaponIndex, int _pickupID, int _playerID, int _ammoLeft, int _clipAmmo)
    {
        GunScript _gunScript;
        
        if (m_Player.gun != null)
        {
            // Disable the last gun
            m_Player.gun.SetActive(false);
            previousPlayerWeapon[_playerID] = m_Player.gun.name.ToLower();

            _gunScript = m_Player.gun.GetComponent<GunScript>();
            
            if (_ammoLeft == -1)
            {
                _ammoLeft = _gunScript.ammoLeft;
                _clipAmmo = _gunScript.clipAmmo;
            }
        }
        else
        {
            previousPlayerWeapon[_playerID] = null;
        }
        
        // Change the current parent weapon socket transform.
        SetSocket(_weaponIndex, m_Player);
        
        // Add the gun first
        AddWeapon(_weaponName, _pickupID, _playerID);
        
        _gunScript = m_Player.gun.GetComponent<GunScript>();
        WeaponTypeIdentifier m_WeaponTypeIdentifier = _gunScript.m_WeaponTypeIdentifier;
        m_Player.muzzleAudioSource = _gunScript.muzzleAudioSource;
        playersAnimators[_playerID] = _gunScript.anim;
        string _actualWeaponName = _gunScript.weaponName;

        // Change the camera to be the one that comes with the arms and weapon
        if (m_Player.isLocal)
        {
            GetNewClipAmmo(m_Player, _gunScript, m_WeaponTypeIdentifier.currentWeaponType, false, _actualWeaponName, _ammoLeft, _clipAmmo);
        }
        else
        {
            Destroy(_gunScript.camera.gameObject);
        }
        
        // Enable the new arms with the gun object
        m_Player.gun.SetActive(true);

        PlayAppropriateAudioAndAnimation(m_Player, _gunScript, _actualWeaponName, "take out");
    }

    public void SwitchWeapons(string _weaponName, int _weaponIndex, int _pickupID, int _playerID, int _ammoLeft=-1, int _clipAmmo=-1)
    {
        PlayerManager m_Player = players[_playerID];
        foreach (KeyValuePair<int, List<GameObject>> _playersBag in playersBags)
        {
            if (_playersBag.Key == _playerID)
            {
                foreach (GameObject _weaponBagObject in _playersBag.Value)
                {
                    WeaponTypeIdentifier m_WeaponTypeIdentifier = _weaponBagObject.GetComponent<WeaponTypeIdentifier>();
                    if (m_WeaponTypeIdentifier.currentWeaponType == _weaponIndex)
                    {
                        // Disable the last gun
                        m_Player.gun.SetActive(false);
                        // Change the current parent weapon socket transform.
                        SetSocket(_weaponIndex, m_Player);
                        previousPlayerWeapon[_playerID] = m_Player.gun.name.ToLower();

                        // Change the arms with the gun object
                        m_Player.gun = _weaponBagObject;
                        GunScript _gunScript = _weaponBagObject.GetComponent<GunScript>();

                        // TODO find an alternative to GetComponent, assign that audio source to a variable
                        m_Player.muzzleAudioSource = _gunScript.muzzleAudioSource;
                        playersAnimators[_playerID] = _gunScript.anim;
                        string _actualWeaponName = _gunScript.weaponName;

                        // Change the camera to be the one that comes with the arms and weapon
                        if (m_Player.isLocal)
                        {
                            if (_ammoLeft == -1)
                            {
                                _ammoLeft = _gunScript.ammoLeft;
                                _clipAmmo = _gunScript.clipAmmo;
                            }

                            GetNewClipAmmo(m_Player, _gunScript, m_WeaponTypeIdentifier.currentWeaponType, false, _actualWeaponName, _ammoLeft, _clipAmmo);
                        }
                        
                        // Enable the new arms with the gun object
                        m_Player.gun.SetActive(true);

                        PlayAppropriateAudioAndAnimation(m_Player, _gunScript, _actualWeaponName, "take out");
                        return;
                    }
                }
                break;
            }
        }

        // It's a new weapon. Add it and switch to it.
        AddThenSwitch(m_Player, _weaponName, _weaponIndex, _pickupID, _playerID, _ammoLeft, _clipAmmo);
    }

    public void DropGun(int _byPlayerID, int _weaponPickupID, Vector3 _dropOrigin, Vector3 _position, Quaternion _rotation, string _currentWeaponName, int _currentSlotID)
    {
        PlayerManager m_Player = players[_byPlayerID];
        PlayerController m_PlayerController = m_Player.playerController;
        GameObject _currentWeapon = m_Player.gun;

        // Switch first
        SwitchWeapons(_currentWeaponName, _currentSlotID, 0, _byPlayerID);
        playersBags[_byPlayerID].Remove(_currentWeapon);

        _position.y -= 1f;
        
        // The spawn a new weapon pickup object
        GameObject _newWeaponPickup = Instantiate(_currentWeapon.GetComponent<WeaponTypeIdentifier>().pickupWeaponPrefab, _position + _dropOrigin, Quaternion.identity);
        weaponPickups[_weaponPickupID] = _newWeaponPickup;

        // Drop the weapon pickup on the ground
        float _throwForce = _newWeaponPickup.GetComponent<WeaponProperties>().throwForce;
        _newWeaponPickup.GetComponent<Rigidbody>().AddForce(_dropOrigin * _throwForce);

        // Destroy the previous weapon and arms
        Destroy(_currentWeapon);
    }

    public void SpawnBullet(int _byPlayerID, Quaternion _rotation, Vector3 _muzzleWorldVelocity)
    {
        PlayerManager m_Player = players[_byPlayerID];
        GunScript _gunScript = m_Player.gun.GetComponent<GunScript>();
        WeaponTypeIdentifier m_WeaponTypeIdentifier = _gunScript.m_WeaponTypeIdentifier;
        Vector3 cameraForward = _gunScript.cameraPlaceholder.transform.forward;
        Vector3 _position = _gunScript.spawnPoints.bulletSpawnPoint.position + _gunScript.spawnPoints.bulletSpawnPoint.forward;
        string _actualWeaponName = _gunScript.weaponName;

        _gunScript.ShootBullet(_position, _rotation, players[_byPlayerID].gameObject, _byPlayerID, cameraForward, _muzzleWorldVelocity);
        
        PlayAppropriateAudioAndAnimation(m_Player, _gunScript, _actualWeaponName, "shoot");

        if (m_Player.isLocal)
        {
            GetNewClipAmmo(m_Player, _gunScript, m_WeaponTypeIdentifier.currentWeaponType, true, _actualWeaponName);
        }
    }

    public void SetStates(int _playerID, Vector3 _newPosition, bool _isGrounded, bool _isSneaking)
    {
        if (previousGroundedState[_playerID] == false && _isGrounded == true)
        {
            AudioManager.instance.PlayLandingSound(_playerID);
            previousGroundedState[_playerID] = _isGrounded;
        }
        else if (previousPlayerPosition[_playerID] != _newPosition)
        {
            StartCoroutine(AudioManager.instance.PlayFootstepSounds(_playerID, _isGrounded, _isSneaking));

            previousGroundedState[_playerID] = _isGrounded;
            previousPlayerPosition[_playerID] = _newPosition;
        }
    }

    public void GunReloaded(int _playerID, bool _isOutOfAmmo)
    {
        PlayerManager m_Player = players[_playerID];
        WeaponTypeIdentifier m_WeaponTypeIdentifier = m_Player.gun.GetComponent<WeaponTypeIdentifier>();
        GunScript _gunScript = m_Player.gun.GetComponent<GunScript>();
        string _actualWeaponName = _gunScript.weaponName;
        float _clipLength;

        if (_isOutOfAmmo)
        {            
            _clipLength = PlayAppropriateAudioAndAnimation(m_Player, _gunScript, _actualWeaponName, "reload empty");
        }
        else
        {            
            _clipLength = PlayAppropriateAudioAndAnimation(m_Player, _gunScript, _actualWeaponName, "reload");
        }

        if (m_Player.isLocal)
        {
            int _ammoLeft = _gunScript.ammoLeft;
            int _clipAmmo = _gunScript.clipAmmo;
            int _maxClipAmmo = _gunScript.maxClipAmmo;

            if (_clipAmmo == _maxClipAmmo || _ammoLeft == 0f)
            return;

            if (_ammoLeft < _maxClipAmmo)
            {
                _clipAmmo = _ammoLeft;
                _ammoLeft = 0;
            }
            else
            {
                _ammoLeft = Mathf.Clamp(_ammoLeft - (_maxClipAmmo - _clipAmmo), 0, 999);
                _clipAmmo = _maxClipAmmo;
            }

            _gunScript.ammoLeft = _ammoLeft;
            _gunScript.clipAmmo = _clipAmmo;

            if (_isOutOfAmmo)
            {
                StartCoroutine(UpdateEmptyClipAmmo(m_Player, _clipLength, m_WeaponTypeIdentifier.currentWeaponType, _actualWeaponName, _ammoLeft, _clipAmmo));
            }
            else
            {
                StartCoroutine(UpdateClipAmmo(m_Player, _clipLength, m_WeaponTypeIdentifier.currentWeaponType, _actualWeaponName, _ammoLeft, _clipAmmo));
            }
        }
    }

    private IEnumerator UpdateClipAmmo(PlayerManager m_Player, float _clipLength, int _currentWeaponType, string _actualWeaponName, int _ammoLeft, int _clipAmmo)
    {
        yield return new WaitForSeconds(_clipLength);
        Reload(m_Player, _currentWeaponType, _actualWeaponName, _ammoLeft, _clipAmmo);
    }

    private IEnumerator UpdateEmptyClipAmmo(PlayerManager m_Player, float _clipLength, int _currentWeaponType, string _actualWeaponName, int _ammoLeft, int _clipAmmo)
    {
        yield return new WaitForSeconds(_clipLength);
        Reload(m_Player, _currentWeaponType, _actualWeaponName, _ammoLeft, _clipAmmo);
    }

    private void Reload(PlayerManager m_Player, int _currentWeaponType, string _actualWeaponName, int _ammoLeft, int _clipAmmo)
    {
        m_Player.playerController.ammoLeftText.text = _ammoLeft.ToString();
        m_Player.playerController.clipAmmoText.text = _clipAmmo.ToString();
    }

    public bool CanReload(int _id)
    {
        PlayerManager m_Player = players[_id];
        GunScript _gunScript = m_Player.gun.GetComponent<GunScript>();

        if (_gunScript.clipAmmo == _gunScript.maxClipAmmo)
        {
            return false;
        }
        else
        {
            return true;
        }
    }
}
