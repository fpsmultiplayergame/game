﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RicochetProjectile : MonoBehaviour
{
    [Header("General")]
    [Tooltip("LifeTime of the projectile")]
    public float maxLifeTime = 5f;
    [Tooltip("A sound source for a ricochet.")]
    public AudioSource audioSource;

    [Header("Movement")]
    [Tooltip("Speed of the projectile")]
    public float speed = 20f;
    [Tooltip("Downward acceleration from gravity")]
    public float gravityDownAcceleration = 0f;
    [Tooltip("Distance over which the projectile will correct its course to fit the intended trajectory (used to drift projectiles towards center of screen in First Person view). At values under 0, there is no correction")]
    public float trajectoryCorrectionDistance = -1;

    private Vector3 m_LasttransformPosition;
    private Vector3 m_Velocity;
    private bool m_HasTrajectoryOverride;
    private Vector3 m_TrajectoryCorrectionVector;
    private Vector3 m_ConsumedTrajectoryCorrectionVector;

    private void OnEnable()
    {
        Destroy(gameObject, maxLifeTime);
    }

    public void StartProjectile(ProjectileBase m_ProjectileBase, Vector3 _cameraPosition, Vector3 _cameraForward)
    {
        m_LasttransformPosition = transform.position;
        m_Velocity = transform.forward * speed;

        // Handle case of player shooting (make projectiles not go through walls, and remember center-of-screen trajectory)-
        m_HasTrajectoryOverride = true;
        
        Vector3 cameraToMuzzle = (m_ProjectileBase.initialPosition - _cameraPosition);

        m_TrajectoryCorrectionVector = Vector3.ProjectOnPlane(-cameraToMuzzle, _cameraForward);
        if (trajectoryCorrectionDistance == 0)
        {
            transform.position += m_TrajectoryCorrectionVector;
            m_ConsumedTrajectoryCorrectionVector = m_TrajectoryCorrectionVector;
        }
        else if (trajectoryCorrectionDistance < 0)
        {
            m_HasTrajectoryOverride = false;
        }

        AudioManager.instance.PlayRicochetSound(this);
    }

    void Update()
    {
        Debug.DrawRay(transform.position, -transform.up * 0.2f, Color.red);
        Debug.DrawRay(transform.position, transform.up * 0.2f, Color.red);
        Debug.DrawRay(transform.position, -transform.forward * 0.2f, Color.red);
        Debug.DrawRay(transform.position, transform.forward * 0.2f, Color.red);
        Debug.DrawRay(transform.position, -transform.right * 0.2f, Color.red);
        Debug.DrawRay(transform.position, transform.right * 0.2f, Color.red);

        // Move
        transform.position += m_Velocity * Time.deltaTime;

        // Drift towards trajectory override (this is so that projectiles can be centered 
        // with the camera center even though the actual weapon is offset)
        if (m_HasTrajectoryOverride && m_ConsumedTrajectoryCorrectionVector.sqrMagnitude < m_TrajectoryCorrectionVector.sqrMagnitude)
        {
            Vector3 correctionLeft = m_TrajectoryCorrectionVector - m_ConsumedTrajectoryCorrectionVector;
            float distanceThisFrame = (transform.position - m_LasttransformPosition).magnitude;
            Vector3 correctionThisFrame = (distanceThisFrame / trajectoryCorrectionDistance) * m_TrajectoryCorrectionVector;
            correctionThisFrame = Vector3.ClampMagnitude(correctionThisFrame, correctionLeft.magnitude);
            m_ConsumedTrajectoryCorrectionVector += correctionThisFrame;

            // Detect end of correction
            if(m_ConsumedTrajectoryCorrectionVector.sqrMagnitude == m_TrajectoryCorrectionVector.sqrMagnitude)
            {
                m_HasTrajectoryOverride = false;
            }

            transform.position += correctionThisFrame;
        }

        // Orient towards velocity
        transform.forward = m_Velocity.normalized;

        // Gravity
        if (gravityDownAcceleration > 0)
        {
            // add gravity to the projectile velocity for ballistic effect
            m_Velocity += Vector3.down * gravityDownAcceleration * Time.deltaTime;
        }

        m_LasttransformPosition = transform.position;
    }
}
