﻿using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine;
using TMPro;

public class PingUpdater : MonoBehaviour
{
    public static PingUpdater instance;
	
    public float pingCountdown = 1f;
    public float pingCountdownLimit = 1f;
    public TextMeshProUGUI pingText;
    public GameObject pingTextObject;
    private double ping;

	public void Awake()
	{
		if(instance == null)
		{
			instance = this;
		}
	}

    public void FixedUpdate()
    {
        if (!pingTextObject.activeInHierarchy && ClientHandle.finishedConnecting)
        {
            pingTextObject.SetActive(true);
        }
        
        pingCountdown += Time.fixedDeltaTime;
        if (pingCountdown >= pingCountdownLimit && ClientHandle.finishedConnecting)
        {
            ping = Client.instance.ping;
            pingCountdown = 0;
            if (ping < 10000)
            {
                pingText.text = ping.ToString() + "ms";
            }
            ClientSend.Ping();
        }
    }
}
