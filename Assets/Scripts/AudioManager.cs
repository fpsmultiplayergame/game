﻿using System;
using System.Collections;
using UnityEngine;

public class AudioManager : MonoBehaviour
{
    public static AudioManager instance;

    [System.Serializable]
	public class SoundClips
	{
        public AudioClip walkingSound;
        public AudioClip landingSound;

		public AudioClip AK47ShootSound;
		public AudioClip AK47TakeOutSound;
		public AudioClip AK47ReloadSoundOutOfAmmo;
		public AudioClip AK47ReloadSoundAmmoLeft;

        public AudioClip glockShootSound;
		public AudioClip glockTakeOutSound;
        public AudioClip glockReloadSoundOutOfAmmo;
		public AudioClip glockReloadSoundAmmoLeft;

		public AudioClip aimSound;
        public AudioClip ricochetSound;
	}
	public SoundClips soundClips;

    [System.Serializable]
	public class SoundClipLengths
	{
        public float OrgAK47ReloadSoundOutOfAmmo;
		public float AK47ReloadSoundOutOfAmmo;
        public float OrgAK47ReloadSoundAmmoLeft;
		public float AK47ReloadSoundAmmoLeft;
        
        public float OrgGlockReloadSoundOutOfAmmo;
        public float glockReloadSoundOutOfAmmo;
		public float OrgGlockReloadSoundAmmoLeft;
        public float glockReloadSoundAmmoLeft;
	}
	public SoundClipLengths soundClipLengths;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.Log("Instance already exists, destroying object!");
            Destroy(this);
        }
    }

    private AudioSource GetAudioSource(int _playerID, bool _muzzle = true)
    {
        PlayerManager m_Player = GameManager.players[_playerID];
        
        if (_muzzle)
        {
            return m_Player.muzzleAudioSource;
        }
        else
        {
            return m_Player.mainAudioSource;
        }
    }
    
    public IEnumerator PlayFootstepSounds(int _playerID, bool _isGrounded, bool _isSneaking)
    {
        AudioSource _audioSource = GetAudioSource(_playerID, false);

        if (!_audioSource.isPlaying)
        {
            if (_isGrounded && !_isSneaking)
            {
                _audioSource.clip = soundClips.walkingSound;
                _audioSource.loop = true;
                _audioSource.Play();
                yield return new WaitForSeconds(0.3f);
                _audioSource.Pause();
                _audioSource.loop = false;
            }
        }
    }

    public void PlayLandingSound(int _playerID)
    {
        AudioSource _audioSource = GetAudioSource(_playerID, false);
        _audioSource.clip = soundClips.landingSound;
        _audioSource.Play();
    }
#region ak-47
    public void PlayAK47TakeOut(int _playerID)
    {
        AudioSource _audioSource = GetAudioSource(_playerID);
        _audioSource.clip = soundClips.AK47TakeOutSound;
        _audioSource.Play();
    }

    public void PlayAK47ShootSound(int _playerID)
    {
        AudioSource _audioSource = GetAudioSource(_playerID);
        _audioSource.clip = soundClips.AK47ShootSound;
        _audioSource.Play();
    }

    public void PlayAK47ReloadSoundOutOfAmmo(int _playerID)
    {
        AudioSource _audioSource = GetAudioSource(_playerID);
        _audioSource.clip = soundClips.AK47ReloadSoundOutOfAmmo;
        _audioSource.Play();
    }

    public void PlayAK47ReloadSoundAmmoLeft(int _playerID)
    {
        AudioSource _audioSource = GetAudioSource(_playerID);
        _audioSource.clip = soundClips.AK47ReloadSoundAmmoLeft;
        _audioSource.Play();
    }
#endregion
    

#region glock
    public void PlayGlockTakeOut(int _playerID)
    {
        AudioSource _audioSource = GetAudioSource(_playerID);
        _audioSource.clip = soundClips.glockTakeOutSound;
        _audioSource.Play();
    }

    public void PlayGlockShootSound(int _playerID)
    {
        AudioSource _audioSource = GetAudioSource(_playerID);
        _audioSource.clip = soundClips.glockShootSound;
        _audioSource.Play();
    }

    public void PlayGlockReloadSoundOutOfAmmo(int _playerID)
    {
        AudioSource _audioSource = GetAudioSource(_playerID);
        _audioSource.clip = soundClips.glockReloadSoundOutOfAmmo;
        _audioSource.Play();
    }

    public void PlayGlockReloadSoundAmmoLeft(int _playerID)
    {
        AudioSource _audioSource = GetAudioSource(_playerID);
        _audioSource.clip = soundClips.glockReloadSoundAmmoLeft;
        _audioSource.Play();
    }
#endregion
    
    public void PlayAimSound(int _playerID)
    {
        AudioSource _audioSource = GetAudioSource(_playerID);
        _audioSource.clip = soundClips.aimSound;
        _audioSource.Play();
    }

    public void PlayRicochetSound(RicochetProjectile _ricochetProjectile)
    {
        AudioSource _audioSource = _ricochetProjectile.audioSource;
        _audioSource.clip = soundClips.ricochetSound;
        _audioSource.Play();
    }
}