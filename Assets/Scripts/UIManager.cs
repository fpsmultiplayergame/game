﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIManager : MonoBehaviour
{
    public static UIManager instance;

    public GameObject camera;
    public GameObject startMenu;
    public GameObject crosshair;
    public InputField usernameField;

    private void Awake()
    {
        if (instance == null)
        {
            instance = this;
        }
        else if (instance != this)
        {
            Debug.Log("Instance already exists, destroying object!");
            Destroy(this);
        }
    }

    private IEnumerator DestroyCamera()
    {
        yield return new WaitForSeconds(1);
        Destroy(camera);
    }

    /// <summary>Attempts to connect to the server.</summary>
    public void ConnectToServer()
    {
        StartCoroutine(DestroyCamera());
        startMenu.SetActive(false);
        crosshair.SetActive(true);
        usernameField.interactable = false;
        Client.instance.ConnectToServer();
    }
}
