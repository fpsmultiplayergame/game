﻿using UnityEngine;
using UnityEngine.Events;

public class ProjectileBase : MonoBehaviour
{
    public GameObject owner { get; private set; }
    public Vector3 initialPosition { get; private set; }
    public Vector3 initialDirection { get; private set; }
    public Vector3 inheritedMuzzleVelocity { get; private set; }

    public UnityAction<int, Vector3, Vector3> onShoot;

    public void Shoot(GameObject controller, int playerId, Vector3 _cameraPosition, Vector3 _cameraForward, Vector3 _muzzleWorldVelocity)
    {
        owner = controller;
        initialPosition = transform.position;
        initialDirection = transform.forward;
        inheritedMuzzleVelocity = _muzzleWorldVelocity;

        if (onShoot != null)
        {
            onShoot.Invoke(playerId, _cameraPosition, _cameraForward);
        }
    }
}
