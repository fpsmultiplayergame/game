﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum WeaponType
{
    Primary,
    Secondary,
    Melee,
    Grenade,
    Smoke,
    Flash,
}

public class WeaponTypeIdentifier : MonoBehaviour
{
    [Tooltip("What type of weapon is this gun? ")]
    public WeaponType weaponType;
    public GameObject pickupWeaponPrefab;
    public int currentWeaponType; // Do not change it to {get; set;} because for some reason it causes bugs lol

    public void SetValue()
    {
        currentWeaponType = GetWeaponType();
    }

    public int GetWeaponType()
    {
        switch (weaponType)
        {
            case WeaponType.Primary:
                return 1;
            case WeaponType.Secondary:
                return 2;
            case WeaponType.Melee:
                return 3;
            case WeaponType.Grenade:
                return 4;
            case WeaponType.Smoke:
                return 5;
            case WeaponType.Flash:
                return 6;
            default:
                Debug.Log("it zzzeeeeeeeeeeeeeeeero");
                return 0;
        }
    } 
}
