﻿using UnityEngine;
using System.Collections;

public class CasingScript : MonoBehaviour
{
	public Rigidbody m_Rigidbody;

	[Header("Force X")]
	[Tooltip("Minimum force on X axis")]
	public float minimumXForce;		
	[Tooltip("Maimum force on X axis")]
	public float maximumXForce;

	[Header("Force Y")]
	[Tooltip("Minimum force on Y axis")]
	public float minimumYForce;
	[Tooltip("Maximum force on Y axis")]
	public float maximumYForce;

	[Header("Force Z")]
	[Tooltip("Minimum force on Z axis")]
	public float minimumZForce;
	[Tooltip("Maximum force on Z axis")]
	public float maximumZForce;

	[Header("Rotation Force")]
	[Tooltip("Minimum initial rotation value")]
	public float minimumRotation;
	[Tooltip("Maximum initial rotation value")]
	public float maximumRotation;

	[Header("Despawn")]
	[Tooltip("Do the casings get destroyed?")]
	public bool despawn;
	[Tooltip("How long after spawning that the casing is destroyed")]
	public float despawnTime;

	[Header("Audio")]
	public AudioClip[] casingSounds;
	public AudioSource audioSource;

	[Header("Spin Settings")]
	//How fast the casing spins
	[Tooltip("How fast the casing spins over time")]
	public float speed = 2500.0f;

	private bool stopSpinning = false;

	private int collisions;

	//Launch the casing at start
	private void Awake () 
	{
		//Random rotation of the casing
		m_Rigidbody.AddRelativeTorque (
			Random.Range(minimumRotation, maximumRotation), //X Axis
			Random.Range(minimumRotation, maximumRotation), //Y Axis
			Random.Range(minimumRotation, maximumRotation)  //Z Axis
			* Time.deltaTime);

		//Random direction the casing will be ejected in
		m_Rigidbody.AddRelativeForce (
			Random.Range (minimumXForce, maximumXForce),  //X Axis
			Random.Range (minimumYForce, maximumYForce),  //Y Axis
			Random.Range (minimumZForce, maximumZForce)); //Z Axis		     
	}

	private void Start () 
	{
		if (despawn)
		{
			//Start the remove/destroy coroutine
			StartCoroutine (RemoveCasing ());
		}
		
		//Set random rotation at start
		transform.rotation = Random.rotation;
	}

	private void FixedUpdate () 
	{
		if (!stopSpinning)
		{
			//Spin the casing based on speed value
			transform.Rotate (Vector3.right, speed * Time.deltaTime);
			transform.Rotate (Vector3.down, speed * Time.deltaTime);
		}
	}

	private void PlaySound () 
	{
		try
		{
			//Get a random casing sound from the array 
			audioSource.clip = casingSounds
				[Random.Range(0, casingSounds.Length)];
			//Play the random casing sound
			audioSource.Play();
		}
		catch (MissingReferenceException) {}
	}

	private IEnumerator RemoveCasing () 
	{
		//Destroy the casing after set amount of seconds
		yield return new WaitForSeconds (despawnTime);
		//Destroy casing object
		Destroy (gameObject);
	}

	private void OnCollisionEnter()
	{
		collisions++;
		
		stopSpinning = false;

		if (maximumYForce >= 1 && collisions <= 2)
		{
			PlaySound();

			//Random direction the casing will be ejected in
			m_Rigidbody.AddRelativeForce (
				0,  //X Axis
				Random.Range (minimumYForce, maximumYForce),  //Y Axis
				0); //Z Axis

			maximumYForce -= 25f;
		}

		stopSpinning = true;
	}

	private void OnCollisionStay()
	{
		stopSpinning = true;
		m_Rigidbody.mass = 1f;
		
		StartCoroutine(WaitForClipToEnd());
	}

	private IEnumerator WaitForClipToEnd ()
	{
		yield return new WaitForSeconds(1f);
		Destroy(audioSource);
	}
}